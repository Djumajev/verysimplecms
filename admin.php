<!DOCTYPE html>
<html>
<head>
	<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	<link rel="stylesheet" href="jquery.cleditor.css" />
	<script src="jquery.min.js"></script>
	<script src="jquery.cleditor.min.js"></script>
	<script  type= text/javascript>
		$(document).ready(function () { $("#input").cleditor(); });
	</script>

<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

<div class = "page">
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		
		$conn = new mysqli($servername, $username, $password, 'db');
		$conn->query( "SET CHARSET utf8" );
		
		if(isset($_GET['nav'])) 
			$navigation = $_GET['nav'];
		else $navigation = 1;
		
		if(isset($_GET['upnav']))
			$update_nav = $_GET['upnav'];
		else $update_nav = 0;
		
		if(isset($_GET['con']))
			$content = $_GET['con'];
		else $content = 0;
		
		if(isset($_GET['lang'])) 
			$language = $_GET['lang'];
		else $language = "en";
		
		if(isset($_GET['newpost'])) 
			$newpost = $_GET['newpost'];
		else $newpost = 0;
		
		if(isset($_GET['newnav'])) 
			$newnav = $_GET['newnav'];
		else $newnav = 0;
	
	session_start();
	if(!isset($_SESSION['login'])){
		header('location: login.php');	
	
	}
	?>
	
	<div class = "header">
		<div class = "navigation">
		<?php 
			if(isset($_POST['save_menu'])){
				$sql = "INSERT INTO page_content_".$language." (Get_ID, Content, Menu) VALUES ('".$navigation."', '".$_POST['save_content']."', '".$_POST['save_menu']."');";
				$conn->query($sql);
			}
			if(isset($_POST['save_nav'])){
				$sql = "INSERT INTO navigation_".$language." (Category) VALUES ('".$_POST['save_nav']."');";
				$conn->query($sql);
			}
			if(isset($_POST['update_menu'])){
				$sql = "UPDATE page_content_".$language." SET Menu = '".$_POST['update_menu']."', Content = '".$_POST['update_content']."' WHERE ID = '".$content."';";
				$conn->query($sql);
			}
			if(isset($_POST['delete'])){
				$sql = "DELETE FROM page_content_".$language." WHERE ID= '".$content."';";
				$conn->query($sql);
			}
			if(isset($_POST['delete_nav'])){
				$sql = "DELETE FROM navigation_".$language." WHERE ID= '".$navigation."';";
				$conn->query($sql);
			}
			if(isset($_POST['update_nav'])){
				$sql = "UPDATE navigation_".$language." SET Category = '".$_POST['update_nav']."' WHERE ID = '".$navigation."';";
				$conn->query($sql);
			}
			
			$sql = "SELECT ID, Category FROM navigation_".$language;
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					echo "<div class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?nav=".$row['ID']."&con=0&lang=".$language."\">".$row['Category']."</a></div>"; //type= text name= navigation value = ".$row['ID'].
				}
			}
			
			echo "<div style = \"float: right; !important\" class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?nav=".$navigation."&con=".$content."&lang=en\">en</a></div>";
			echo "<div style = \"float: right; !important\" class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?nav=".$navigation."&con=".$content."&lang=ru\">ru</a></div>";
			echo "<div style = \"float: right; !important\" class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?nav=".$navigation."&con=".$content."&lang=lv\">lv</a></div>";
			if($language == "en") echo "<div class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?newnav=1&lang=".$language."\">New navigation</a></div>"; 
			if($language == "ru") echo "<div class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?newnav=1&lang=".$language."\">Новая навигация</a></div>"; 
			if($language == "lv") echo "<div class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?newnav=1&lang=".$language."\">Jauna navigacija</a></div>"; 
			
			if($language == "en") echo "<div class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?upnav=1&lang=".$language."\">Update navigation</a></div>"; 
			if($language == "ru") echo "<div class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?upnav=1&lang=".$language."\">Изменить навигацию</a></div>"; 
			if($language == "lv") echo "<div class = \"navigation_buttons\"><a class = \"navigation_a\" href = \" admin.php?upnav=1&lang=".$language."\">Redaktēt navigaciju</a></div>";
		?>
		</div>
	</div>

	<div class = "menu">
	<?php
		if(!$newnav && !$update_nav){
			$sql = "SELECT * FROM page_content_".$language." WHERE Get_ID = ".$navigation;
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					echo "<div class = \"menu_buttons\"><a href = \" admin.php?nav=".$row['Get_ID']."&con=".$row['ID']."&lang=".$language."\" class = \"menu_a\">".$row['Menu']."</a></div>";
				}
			}
			if($language == "en") echo "<div class = \"menu_buttons\"><a href = \" admin.php?newpost=1&lang=".$language."&nav=".$navigation."\" class = \"menu_a\">Create New</a></div>";
			if($language == "lv") echo "<div class = \"menu_buttons\"><a href = \" admin.php?newpost=1&lang=".$language."&nav=".$navigation."\" class = \"menu_a\">Izvaidīt jauno</a></div>";
			if($language == "ru") echo "<div class = \"menu_buttons\"><a href = \" admin.php?newpost=1&lang=".$language."&nav=".$navigation."\" class = \"menu_a\">Создать новую</a></div>";
		}
		else if($update_nav){
			$sql = "SELECT * FROM navigation_".$language;
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					echo "<div class = \"menu_buttons\"><a href = \" admin.php?nav=".$row['ID']."&lang=".$language."&upnav=1\" class = \"menu_a\">".$row['Category']."</a></div>";
				}
			}
		}
	?>
	</div>

	<div class = "content">
	<?php
		$check = false;
		
		if($content < 1 && !$update_nav) {
			$sql = "SELECT * FROM page_content_".$language." WHERE Get_ID=".$navigation;
		} else if(!$update_nav){
			$sql = "SELECT * FROM page_content_".$language." WHERE ID = ".$content;
			$check = true;
		}
		else $sql = "SELECT Category FROM navigation_".$language." WHERE ID= ".$navigation.";";
		
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				if($check == true && !$newpost && !$newnav){
					echo "<form method=POST><textarea type = text name = update_menu style = \"width: 100%;\">".$row['Menu']."</textarea>";
					echo "<textarea id = \"input\" type = text name = update_content style = \"width: 100%; min-height: 100em;\">".$row['Content']."</textarea>";
					if($language == "en") echo "<input type=submit value= save style=\"margin-right: 5em;\"><input type=submit name = delete value= delete></form>";
					if($language == "ru") echo "<input type=submit value= сохранить style=\"margin-right: 5em;\"><input type=submit name = delete value= удалить></form>";
					if($language == "lv") echo "<input type=submit value= Saglabāt style=\"margin-right: 5em;\"><input type=submit name = delete value= izdzēst></form>";
				}
				else if(!$newpost && !$newnav && !$update_nav) echo "<div style = \"font-size: 1.5em;\">".$row['Menu']."</div><p>[..]</p>";
				else if($update_nav){
					echo "<form method=POST><textarea type = text name = update_nav style = \"width: 100%; min-height: 100em;\">".$row['Category']."</textarea>";
					if($language == "en") echo "<input type=submit value= save style=\"margin-right: 5em;\"><input type=submit name = delete_nav value= delete></form>";
					if($language == "ru") echo "<input type=submit value= сохранить style=\"margin-right: 5em;\"><input type=submit name = delete_nav value= удалить></form>";
					if($language == "lv") echo "<input type=submit value= Saglabāt style=\"margin-right: 5em;\"><input type=submit name = delete_nav value= izdzēst></form>";
				}
			}
		}
		$conn->close();
		if($newpost && !$newnav){
			echo "<form method=POST><textarea type = text name = save_menu style = \"width: 100%;\">Menu</textarea>";
			echo "<textarea type = text id = \"input\" name = save_content style = \"width: 100%; min-height: 30em;\">Content</textarea>";
			if($language == "en") echo "<input type=submit value= save></form>";
			if($language == "ru") echo "<input type=submit value= сохранить></form>";
			if($language == "lv") echo "<input type=submit value= Saglabāt></form>";
		}
		else if($newnav){
			echo "<form method=POST><textarea type = text name = save_nav style = \"width: 100%;\">Category</textarea>";
			if($language == "en") echo "<input type=submit value= save></form>";
			if($language == "ru") echo "<input type=submit value= сохранить></form>";
			if($language == "lv") echo "<input type=submit value= Saglabāt></form>";
		}
	?>
	</div>

	<div class = "footer">
		<p>Footer test</p>
	</div>
	
</div>
	
</body>
</html>