-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 11, 2018 at 07:07 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `ID` int(10) UNSIGNED NOT NULL,
  `user_name` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`ID`, `user_name`, `password`) VALUES
(1, 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `navigation_en`
--

CREATE TABLE `navigation_en` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Category` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `navigation_lv`
--

CREATE TABLE `navigation_lv` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Category` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `navigation_ru`
--

CREATE TABLE `navigation_ru` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Category` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_content_en`
--

CREATE TABLE `page_content_en` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Get_ID` int(10) UNSIGNED DEFAULT NULL,
  `Menu` text COLLATE utf8mb4_unicode_ci,
  `Content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_content_lv`
--

CREATE TABLE `page_content_lv` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Get_ID` int(10) UNSIGNED DEFAULT NULL,
  `Menu` text COLLATE utf8mb4_unicode_ci,
  `Content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_content_ru`
--

CREATE TABLE `page_content_ru` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Get_ID` int(10) UNSIGNED DEFAULT NULL,
  `Menu` text COLLATE utf8mb4_unicode_ci,
  `Content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `navigation_en`
--
ALTER TABLE `navigation_en`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `navigation_lv`
--
ALTER TABLE `navigation_lv`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `navigation_ru`
--
ALTER TABLE `navigation_ru`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `page_content_en`
--
ALTER TABLE `page_content_en`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `page_content_lv`
--
ALTER TABLE `page_content_lv`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `page_content_ru`
--
ALTER TABLE `page_content_ru`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `navigation_en`
--
ALTER TABLE `navigation_en`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `navigation_lv`
--
ALTER TABLE `navigation_lv`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `navigation_ru`
--
ALTER TABLE `navigation_ru`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_content_en`
--
ALTER TABLE `page_content_en`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_content_lv`
--
ALTER TABLE `page_content_lv`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_content_ru`
--
ALTER TABLE `page_content_ru`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
